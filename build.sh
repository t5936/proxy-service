#!/bin/bash

set -e

if ! [ -x "$(command -v go)" ]; then
    echo "go is not installed"
    exit 1
fi
if ! [ -x "$(command -v git)" ]; then
    echo "git is not installed"
    exit 1
fi
if [ -z "${GOPATH}" ]; then
    echo "set GOPATH"
    exit 1
fi

export GO111MODULE=on

go get github.com/golang/protobuf/protoc-gen-go
go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
go mod tidy
go mod verify
go mod vendor
go install -v ./...

packs=("api" "user" "project" "task");

# Generate go protobuf files for all protos
for d in ${packs[@]} ; do
    echo "Compiling $d";
    protoc -I . -I proto \
            --go_out=. --go_opt=paths=source_relative \
            --go-grpc_out=. --go-grpc_opt=paths=source_relative \
            proto/$d/*.proto
done

